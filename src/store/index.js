import { persistStore } from 'redux-persist';
import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import rootSaga from './sagas';
import persistReducers from './persistReducers';


const middlewares = [];

const sagaMonitor = __DEV__ ? console.tron.createSagaMonitor() : null
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

middlewares.push(sagaMiddleware);

const composer = __DEV__
    ? compose(
        applyMiddleware( ...middlewares),
        console.tron.createEnhancer(),
    )
    : applyMiddleware( ...middlewares);



//store mantem o estado global do app
const store = createStore(persistReducers(rootReducer), composer);
const persistor = persistStore(store)

sagaMiddleware.run(rootSaga);

if (__DEV__) console.tron.log(store.getState());
export { store, persistor };
