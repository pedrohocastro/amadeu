export const dadosInvestimentosRequest = () => ({
    type: 'DADOS_INVESTIMENTOS_REQUEST',
})

export const dadosInvestimentosSuccess = (data) => ({
    type: 'DADOS_INVESTIMENTOS_SUCCESS',
    payload: { data},
})

export const dadosInvestimentosFailure = () =>({
    type: 'DADOS_INVESTIMENTOS_FAILURE',
})

export const limparInvestimentos = () => ({
    type: 'LIMPAR_INVESTIMENTOS',
})


