import AsyncStorage from '@react-native-community/async-storage';

// import storage from 'redux-persist/lib/storage';
import { persistReducer } from "redux-persist";
// import { pe }

 export default reducers => {
     const persistedReducer = persistReducer(
         {
            key: 'happe', //mudar essa chave para cada app
            storage: AsyncStorage,
            whitelist:['login']  //aqui você deve colocar todos os reducers que devem ser persistidos
        },
        reducers
     );
     return persistedReducer;
 }
