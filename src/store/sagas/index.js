import { all, takeLatest, call, put } from 'redux-saga/effects';
import { api } from './../../services/api';
import * as InvestimentosActions from './../actions/investimentos';

import { Alert } from 'react-native';



function* loadInvestimentos(){


    try{

        let resposta = yield call(api.post,'/v2/5e76797e2f0000f057986099');

        let data = resposta.data.response.data.listaInvestimentos;

        yield put(InvestimentosActions.dadosInvestimentosSuccess(data));


      }catch(err){

        Alert.alert("Ops!", err.message)
        yield put(InvestimentosActions.dadosInvestimentosFailure());
      }
}


export default function* rootSaga(){

    return yield all([

        takeLatest('DADOS_INVESTIMENTOS_REQUEST', loadInvestimentos),


    ]); //


}
