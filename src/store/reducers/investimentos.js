const INITIAL_STATE = {
    data: [],
    erro: false,
    loading: false,
    sucesso: false,
}
//reducers -> Retorna e manipula o estado da aplicação
//Parametros -> Estado atual (e pode retornarmodificado) e o action (ações que posso realizar no estado)
function investimentos (state = INITIAL_STATE, action) {

    switch(action.type){

        case 'DADOS_INVESTIMENTOS_REQUEST':
            return {...state, loading: true, }
        case 'DADOS_INVESTIMENTOS_SUCCESS':
            return {...state, data: action.payload.data, erro: false, loading: false, sucesso: true }
        case 'DADOS_INVESTIMENTOS_FAILURE':
            return {...state, erro: true, loading: false, }
        case 'LIMPAR_INVESTIMENTOS':
            return INITIAL_STATE
        default:
            return state;
    }
}

export default investimentos;
