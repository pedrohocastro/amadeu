import styled from 'styled-components/native';
import { cores } from '../../styles';

export const Container = styled.View`
    flex:1;
    background-color: ${cores.brancoApp};
    justify-content: space-between;
`;

export const TituloSessao = styled.Text`
    margin-top: 20px;
    margin-bottom: 20px;
    padding: 0px 20px;
    justify-content: center;
    color: ${cores.cinza};
    font-weight: 600;
`;

export const BotaoConfirmarResgate = styled.TouchableOpacity`
    width: 100%;
    height: 60px;
    background-color: ${cores.amarelo};
    align-items: center;
    justify-content: center;
`;

export const TituloBotaoConfirmar = styled.Text`
    font-size: 18px;
    color: ${cores.azulPrincipal};
    font-weight: bold;
`;
