import React, { useState } from 'react';
import { View, FlatList, KeyboardAvoidingView } from 'react-native';

import { Container, TituloSessao, BotaoConfirmarResgate, TituloBotaoConfirmar } from './styles';
import HeaderPrincipal from '../../components/Headers/HeaderPrincipal';
import CardResgate from '../../components/Cards/CardResgate';
import ModalResgateEfetuado from '../../components/Modais/ModalResgateEfetuado';
import { useDispatch, useSelector } from 'react-redux';
import { formatReal, calculaPercentagem } from '../../utils';
import CardResgateInput from '../../components/Cards/CardResgateInput';

export default function Resgate({route}) {
    const [modalResgate, setModalResgate] = useState(false);
    const dispatch = useDispatch();
    const loading = useSelector(state=>state.investimentos.loading);
    const investimento = route.params.item;
    const acoes = investimento.acoes;

    function mostrarModalResgate(){
        setModalResgate(!modalResgate);
    }
  return (
    <Container>
        <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : ''} enabled>
            <HeaderPrincipal titulo="Resgate" />
            <TituloSessao>DADOS DO INVESTIMENTO</TituloSessao>
            <CardResgate titulo={"Nome"} descricao={investimento.nome} />
            <CardResgate titulo="Saldo total disponível" descricao={ formatReal(investimento.saldoTotalDisponivel)} />
            <TituloSessao>RESGATE DO SEU JEITO</TituloSessao>
            <FlatList
                data={acoes}
                renderItem={({ item }) => {
                    return <View style={{marginBottom: 10}}>
                                <CardResgate titulo="Ação" descricao={item.nome} />
                                <CardResgate titulo="Saldo Acumulado" descricao={formatReal(calculaPercentagem(item.percentual, investimento.saldoTotalDisponivel))} />
                                <CardResgateInput valorMaximo={calculaPercentagem(item.percentual, investimento.saldoTotalDisponivel)}/>
                           </View>
                }
            }
                keyExtractor={item => item.id}
                ListFooterComponent={<View style={{height:40}} />}
            />

        </KeyboardAvoidingView>
        <BotaoConfirmarResgate onPress={mostrarModalResgate}>
            <TituloBotaoConfirmar>CONFIRMAR RESGATE</TituloBotaoConfirmar>
        </BotaoConfirmarResgate>
        <ModalResgateEfetuado
            visivel={modalResgate}
            onClose={mostrarModalResgate}
        />
    </Container>
  );
}
