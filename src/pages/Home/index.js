 import React, { useEffect } from 'react';
 import { View, Text, ActivityIndicator } from 'react-native';
//  import Icon from 'react-native-vector-icons/MaterialIcons'
 import { Container } from './styles';
import HeaderPrincipal from '../../components/Headers/HeaderPrincipal';
import ListaInvestimentos from '../../components/Listas/ListaInvestimentos';
import { useDispatch, useSelector } from 'react-redux';
import { dadosInvestimentosRequest } from './../../store/actions/investimentos'

 export default function Home() {
    const dispatch = useDispatch();
    const loading = useSelector(state=>state.investimentos.loading);

    useEffect(()=>{
        dispatch(dadosInvestimentosRequest())
    },[])

   return (
       <Container>
           <HeaderPrincipal titulo={'Resgate'} />
           {loading
                ?
                    <ActivityIndicator size='large' />
                :
                    <ListaInvestimentos />

           }
       </Container>
   );
 }
