import styled from 'styled-components/native';

export const Container = styled.View`
  flex:1;
  background: #F0F0F0;
  align-items: center;
  justify-content: center;
`;
