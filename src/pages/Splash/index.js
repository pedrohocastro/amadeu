import React, { useEffect } from 'react';
import { View, Image, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { Container } from './styles';

export default function Splash() {
    const { replace } = useNavigation();

    useEffect(()=>{
        //aqui validar cookie e mandar pra tela de login ou home
        setTimeout(() => {
            replace('Home')
        }, 3000);
    },[])

    return (
        <Container>
            <Image source={require('./../../assets/imagens/logo.png')} />
        </Container>
    );
}

