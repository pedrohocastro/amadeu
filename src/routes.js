
import * as React from 'react';
// import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './pages/Home';
import Splash from './pages/Splash';
import Resgate from './pages/Resgate';

const Stack = createStackNavigator();



function Routes() {

  return (
    // <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash" screenOptions={{headerStyle: { backgroundColor: 'rgba(23,37,61,0.9)' }, headerTintColor: '#FFF', headerShown: false}} >
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Resgate" component={Resgate} />
      </Stack.Navigator>
    // </NavigationContainer>
  );
}

export default Routes;
