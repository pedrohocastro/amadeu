import React from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { View, Text} from 'react-native';

import './config/ReactotronConfig';

import Routes from './routes';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import {store, persistor } from './store';
// import { Container } from './styles';

//  import Routes from './src/routes'
export default function App() {
  return (
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <NavigationContainer>
                <Routes/>
            </NavigationContainer>
        </PersistGate>
    </Provider>
  );
}
