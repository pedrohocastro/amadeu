import { Dimensions, Platform } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';

const { width, height } = Dimensions.get('window');


export default{
    screenWidth: width <  height ? width :  height,
    screenHeight:  width <  height ? height :  width,
    ...Platform.select({
        ios: {
            ...ifIphoneX({
                headerHeight: 85,
                headerPadding: 20
            },{
                headerHeight:65, headerPadding: 20
            }

            )},
        android: { headerHeight: 65,headerPadding: 0 },

    })
}
