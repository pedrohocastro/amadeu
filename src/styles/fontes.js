export default {
    Redgar: "Redgar",
    LatoBlack: 'Lato-Black',
    LatoBold: 'Lato-Bold',
    LatoLight: 'Lato-Light',
    LatoRegular: 'Lato-Regular'
  };