export default {
    laranjaPrincipal: 'rgba(241,101,1,1)',
    azulPrincipal: 'rgba(11,92,163,1)',
    azulFacebook: 'rgba(66,103,178,1)',
    preto: 'rgba(57,60,69,1)',
    cinza: 'rgba(133,133,133,1)',
    cinzaClaro: 'rgba(142,142,147,1)',
    branco: 'rgba(255,255,255,1)',
    brancoApp: 'rgba(244,244,244,1)',
    amarelo: 'rgba(248,226,87,1)',

    transparente: 'transparent',

    erro: 'rgba(239,90,36,1)',


    pretoTransparente: 'rgba(0,0,0,.4)',
    brancoTransparente: 'rgba(255,255,255,.4)',

    sombra: 'rgba(0, 0, 0, 0.16)',


}
