import Reactotron, { asyncStorage } from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import reactotronSaga from 'reactotron-redux-saga'
import { NativeModules } from 'react-native';
import { AsyncStorage } from '@react-native-community/async-storage';
// const host = NativeModules.SourceCode.scriptURL.split('://')[1].split(':')[0];

if (__DEV__){
    const tron = Reactotron
    .configure() // controls connection & communication settings
    .use(asyncStorage())
    .use(reactotronRedux()) // add all built-in react native plugins
    .use(reactotronSaga())
    .connect() // let's connect!

    tron.clear();
    console.tron = tron;
}
