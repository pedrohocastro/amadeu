import styled from 'styled-components/native';
import { metricas, cores } from './../../../styles';

export const Container = styled.View`
    background-color: ${cores.azulPrincipal};
    width: 100%;
    height: ${metricas.headerHeight}px;
    border-bottom-width: 5px;
    border-color: ${cores.amarelo};
    align-items: center;
    padding-bottom: 10px;
    justify-content: flex-end;
`;

export const Titulo = styled.TextInput`
    font-size: 18px;
    color: ${cores.branco};
    font-weight: bold;
`;
