import React from 'react';
import { View } from 'react-native';

import { Container, Titulo } from './styles';

export default function HeaderPrincipal({titulo}) {
  return (
    <Container>
        <Titulo>{titulo}</Titulo>
    </Container>
  );
}

HeaderPrincipal.defaultProps = {
    titulo: 'Header Principal'
}
