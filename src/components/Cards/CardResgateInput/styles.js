import styled from 'styled-components/native';
import { cores } from '../../../styles';

export const Container = styled.KeyboardAvoidingView`
    background-color: ${cores.branco};
    height: 70px;
    width: 100%;
    justify-content: space-between;
    border-bottom-width: 1px;
    border-color: ${cores.brancoApp};
    padding: 10px 20px;


`;

export const ContainerTextoInicial = styled.View`
    flex-direction: row;
    align-items: center;
`;


export const Titulo = styled.Text`
    font-size: 13px;
    font-weight: 600;
`;

export const Erro = styled.Text`
    font-size: 13px;
    color: #f00;
    font-weight: 600;
`;

export const Cifrao = styled.Text`
    font-size: 15px;
    font-weight: 600;
`;


export const Descricao = styled.TextInput`
    font-size: 17px;
    font-weight: 600;
    margin-left:10px;
    width: 100%;
`;
