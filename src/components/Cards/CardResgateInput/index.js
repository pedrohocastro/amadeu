import React,{useState} from 'react';
import { View, Text, } from 'react-native';
import StringMask from "string-mask";

import { Container, ContainerTextoInicial, ContainerTextoFinal, Erro, Titulo, Descricao, Cifrao } from './styles';
import { formatReal } from '../../../utils';

const mask = new StringMask("###.###.##0,00", { reverse: true });

export default function CardResgateInput({titulo, valorMaximo}) {

    const [valor, setValor] = useState('');
    const [erro, setErro] = useState('');
    function limparStr(str){
        return str.replace(/[.,]+/g, "");
    };
  return (
    <Container>
            <Titulo>{titulo}</Titulo>
            <ContainerTextoInicial>
                <Cifrao>R$</Cifrao>
                <Descricao
                    placeholder={'0,00'}
                    keyboardType='numeric'
                    value={mask.apply(valor)}
                    onChangeText={(state)=>{
                        if (valor > valorMaximo){
                            setErro(`Valor não pode ser maior que ${formatReal(valorMaximo)} `);
                        }else{
                            setErro('');
                        }
                        setValor(parseInt(limparStr(state)).toString());
                    }}
                />
            </ContainerTextoInicial>
            <Erro>{erro}</Erro>
    </Container>
  );
}

CardResgateInput.defaultProps = {
    titulo: 'Titulo',
    descricao: 'Descricao',
}
