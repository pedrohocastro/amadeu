import React from 'react';
import { View } from 'react-native';

import { Container, ContainerTextoInicial, ContainerTextoFinal, Titulo, Descricao } from './styles';

export default function CardResgate({titulo, descricao}) {
  return (
    <Container>
        <ContainerTextoInicial>
            <Titulo>{titulo}</Titulo>
            <Descricao>{descricao}</Descricao>
        </ContainerTextoInicial>
    </Container>
  );
}

CardResgate.defaultProps = {
    titulo: 'Titulo',
    descricao: 'Descricao',
}
