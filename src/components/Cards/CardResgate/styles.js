import styled from 'styled-components/native';
import { cores } from '../../../styles';

export const Container = styled.View`
    background-color: ${cores.branco};
    height: 50px;
    width: 100%;
    justify-content: center;
    border-bottom-width: 1px;
    border-color: ${cores.brancoApp};

`;

export const ContainerTextoInicial = styled.View`
    padding: 0px 20px;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`;

export const ContainerTextoFinal = styled.View`
    padding: 0px 20px;
    flex-direction: row;
    justify-content: space-between;
`;

export const Titulo = styled.Text`
    font-size: 16px;
    font-weight: 600;
`;

export const Descricao = styled.Text`
    color: rgba(133,133,133,1);
    font-size: 17px;
    font-weight: 600;
`;
