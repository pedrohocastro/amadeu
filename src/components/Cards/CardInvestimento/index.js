import React from 'react';
import { View } from 'react-native';

import { Container, ContainerTextoInicial, ContainerTextoFinal, Titulo, Descricao } from './styles';
import { formatReal } from '../../../utils';
import { useNavigation } from '@react-navigation/native';

export default function CardInvestimento({item}) {
    const navigation = useNavigation();
    function irParaResgatar(){
        navigation.navigate('Resgate', {item});
    }

  return (
    <Container onPress={irParaResgatar}>
        <ContainerTextoInicial>
            <Titulo>{item.nome}</Titulo>
            <Titulo>{ formatReal(item.saldoTotalDisponivel)}</Titulo>
        </ContainerTextoInicial>
        <ContainerTextoFinal>
            <Descricao>{item.objetivo}</Descricao>
        </ContainerTextoFinal>
    </Container>
  );
}

CardInvestimento.defaultProps={
    item: {},
}
