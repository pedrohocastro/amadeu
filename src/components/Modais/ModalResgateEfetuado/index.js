import React,{ useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { Container, ContainerView, ContainerBranco, ModalOverlay, Titulo, Descricao, BotaoNovoResgate, TituloBotao } from './styles';


export default function ModalResgateEfetuado({visivel, onClose}) {

  return (

                <Container
                    animationType='fade'
                    transparent={true}
                    visible={visivel}>
                    <ContainerView >
                        <ModalOverlay onPress={onClose}/>
                        <ContainerBranco>
                            <>
                                <Titulo>RESGATE EFETUADO!</Titulo>
                                <Descricao>O valor solicitado estará em sua{'\n'}conta em até 5 dias úteis</Descricao>
                            </>
                            <BotaoNovoResgate>
                                <TituloBotao>NOVO RESGATE</TituloBotao>
                            </BotaoNovoResgate>
                        </ContainerBranco>
                    </ContainerView>
                </Container>

    );
}
