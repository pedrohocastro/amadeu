import styled from 'styled-components/native';
import { cores, metricas } from '../../../styles';

export const Container = styled.Modal`

`;

export const ContainerView = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;

`;

export const ModalOverlay = styled.TouchableOpacity`
        position: absolute;
        background-color: rgba(19,35,57,0.9);
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        /* width: ${metricas.screenWidth}px; */
        /* height: ${metricas.screenHeight}px; */
        align-items: center;
`;


export const ContainerBranco = styled.View`
        align-items: center;
        background-color: ${cores.branco};

        width: 90%;
        padding: 20px 0px 0px 0px;
        bottom:20px;
        margin: 20px;
        z-index: 10;

`;

export const Titulo = styled.Text`
    font-size: 20px;
    font-weight: 700;
    color: ${cores.azulPrincipal};
`;

export const Descricao = styled.Text`
    font-size: 16px;
    text-align: center;
    margin-top:10px;
    margin-bottom: 40px;
    color: ${cores.azulPrincipal};
`;

export const BotaoNovoResgate = styled.TouchableOpacity`
    height: 50px;
    background-color: ${cores.amarelo};
    justify-content: center;
    align-items: center;
    width: 100%;
`;

export const TituloBotao = styled.Text`
    font-size: 16px;
    font-weight: 700;

    color: ${cores.azulPrincipal};
`;
