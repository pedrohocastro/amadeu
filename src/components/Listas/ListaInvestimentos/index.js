import React from 'react';
import { View, FlatList } from 'react-native';

import { Container, ContainerTitulos, Titulo } from './styles';
import CardInvestimento from '../../Cards/CardInvestimento';
import { useSelector } from 'react-redux';
import { formatReal } from '../../../utils';
import { useNavigation } from '@react-navigation/native';

export default function ListaInvestimentos() {

  const data = useSelector(state=>state.investimentos.data);

  return (
    <Container>
        <ContainerTitulos>
            <Titulo>INVESTIMENTOS</Titulo>
            <Titulo>R$</Titulo>
        </ContainerTitulos>
        <FlatList
            data={data}
            renderItem={({ item }) => <CardInvestimento item={item} />}
            keyExtractor={item => item.nome}
        />

    </Container>
  );
}
