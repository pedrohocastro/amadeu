import styled from 'styled-components/native';
import { cores } from '../../../styles';

export const Container = styled.View`
    background-color: ${cores.brancoApp};
    width: 100%;
`;

export const ContainerTitulos = styled.View`
    background-color: ${cores.brancoApp};
    flex-direction: row;
    padding: 20px;
    justify-content: space-between;
    width: 100%;
`;

export const Titulo = styled.Text`
    color: rgba(133,133,133,1);
    font-weight: 600;
`;


