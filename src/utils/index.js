
  export const paraReal = double => {

      return double.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
  };

  export const isInvalidCPF = (strCPF, campo) => {
    const condicao =
    strCPF === "" || strCPF.length === 0 || !strCPF || isEmpty(strCPF);
    if (condicao) {
      //tratar como campo obrigatórrio
      return true;
    }

    let allTheSame = true;
    for(let i = 1; i < strCPF.length; i++)
    {
        if(strCPF[i] !== strCPF[0])
            allTheSame = false;
    }

    if (!this.validaCPF(strCPF) || allTheSame){
      //tratar como cpf inválido
      return true;
    }
    return false;

  }

  export const isInvalidEmail = (email) => {
    const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    return !(expression.test(String(email).toLowerCase()))
}


  export const isInvalidTelefone = (strCPF, campo) => {
    if (__DEV__) console.tron.log("invalido =>",strCPF);
    const condicao =
    strCPF === "" || strCPF.length < 10 || !strCPF || isEmpty(strCPF);
    if (condicao) {
      //tratar como campo obrigatórrio
      return true;
    }
    return false;

  }

  validaCPF = strCPF => {

    let Soma = 0;
    let Resto;

    let i = 1;

    if (strCPF == "00000000000") return false;

    for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

      if ((Resto == 10) || (Resto == 11))  Resto = 0;
      if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

    Soma = 0;
      for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
      Resto = (Soma * 10) % 11;

      if ((Resto == 10) || (Resto == 11))  Resto = 0;
      if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
      return true;
  }

  export function isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  export function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}
export function formatDecimal(valor, casasDecimais, down) {
    if ((!valor && valor != 0) || valor === "")
        return "";
    if (typeof casasDecimais === "undefined") {
        casasDecimais = 2;
    }
    if (down) {
        return "" + Number((Math.floor(valor * 100) / 100)).toFixed(casasDecimais).replace(/\,/g, "|").replace(/\./g, ",").replace(/\|/g, ".");
    }
    return "" + Number(valor).toFixed(casasDecimais).replace(/\,/g, "|").replace(/\./g, ",").replace(/\|/g, ".");
}
export function formatMilharBrasil(valor) {
    valor += '';
    var x = valor.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
export function formatReal(valor, casasDecimais, down) {
    if ((!valor && valor != 0) || valor === "")
        return "";
    return "R$ " + formatMilharBrasil(formatDecimal(valor, casasDecimais, down));
}

export function calculaPercentagem(percentagem, total){

    return (percentagem * total) / 100

}
