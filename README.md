# Template React Native Happe v1.0.2

### Libs

Estão configuradas as seguintes libs:

* [React Navigation](https://reactnavigation.org/) - Para navegação, configurada para v5
* [Axios](https://github.com/axios/axios) - Para requisições HTTP
* [Redux](https://redux.js.org/) - Container global
* [Redux Persist](https://github.com/rt2zz/redux-persist) - Persistência local de dados
* [Redux Saga](https://github.com/redux-saga/redux-saga) - Para lidar com requisições assíncronas
* [Immer](https://github.com/immerjs/immer) - Facilita na modificação de um estado do Redux
* [React Native iPhone X Helper](https://www.npmjs.com/package/react-native-iphone-x-helper) - Para tratar celulares com notch.
* [React Native Vector Icons](https://github.com/oblador/react-native-vector-icons) - Biblioteca de ícones (CONFIGURADO PARA MATERIAL ICONS)
* [Styled Components](https://styled-components.com/) - Para estilização
* [String Mask](https://www.npmjs.com/package/string-mask) - Para máscaras em String(CPF,Telefone...)
* [Reactotron](https://github.com/infinitered/reactotron) - Para debug, controle de estado, snapshots...
* [Reactotron Redux Saga](https://github.com/infinitered/reactotron/blob/master/docs/plugin-redux-saga.md) - Para debug de requisições
* [Async Storage](https://github.com/react-native-community/async-storage) - Persistência de dados

### Como usar
npx react-native init NomeDoProjeto --template happe

cd NomeDoProjeto

npx react-native run-ios

npx react-native run-android

Se for necessário rodar:

cd ios

pod install

cd ..

npx react-native run-ios

